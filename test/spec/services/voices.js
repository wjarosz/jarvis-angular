'use strict';

describe('Service: Voices', function () {

  // load the service's module
  beforeEach(module('jarvisAngularYoApp'));

  // instantiate service
  var Voices;
  beforeEach(inject(function (_Voices_) {
    Voices = _Voices_;
  }));

  it('should do something', function () {
    expect(!!Voices).toBe(true);
  });

});

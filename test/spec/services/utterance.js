'use strict';

describe('Service: utterance', function () {

  // load the service's module
  beforeEach(module('jarvisAngularYoApp'));

  // instantiate service
  var utterance;
  beforeEach(inject(function (_utterance_) {
    utterance = _utterance_;
  }));

  it('should do something', function () {
    expect(!!utterance).toBe(true);
  });

});

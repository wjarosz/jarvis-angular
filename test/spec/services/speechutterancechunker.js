'use strict';

describe('Service: speechUtteranceChunker', function () {

  // load the service's module
  beforeEach(module('jarvisAngularYoApp'));

  // instantiate service
  var speechUtteranceChunker;
  beforeEach(inject(function (_speechUtteranceChunker_) {
    speechUtteranceChunker = _speechUtteranceChunker_;
  }));

  it('should do something', function () {
    expect(!!speechUtteranceChunker).toBe(true);
  });

});

'use strict';

describe('Controller: CommandeditCtrl', function () {

  // load the controller's module
  beforeEach(module('voictekApp'));

  var CommandeditCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    CommandeditCtrl = $controller('CommandeditCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});

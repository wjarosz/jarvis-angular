'use strict';

/**
 * @ngdoc service
 * @name jarvisAngularYoApp.fileUpload
 * @description
 * # fileUpload
 * Service in the jarvisAngularYoApp.
 */
angular.module('jarvisAngularYoApp')
  .service('Fileupload', ['$http', function ($http) {

        this.uploadFileToUrl = function(file, uploadUrl){
            var fd = new FormData();
            fd.append('file', file);

            $http.post(uploadUrl, fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            })
                .success(function(){
                   return {'message':'New plugin uploaded!','success':true, errors:{}};

                })
                .error(function(){
                   return {'message':'Error with upload!','success':false, errors:{}};
                });
        };

    }]);
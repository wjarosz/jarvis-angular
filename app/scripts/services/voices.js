'use strict';

/**
 * @ngdoc service
 * @name jarvisAngularYoApp.Voices
 * @description
 * # Voices
 * Factory in the jarvisAngularYoApp.
 */
angular.module('jarvisAngularYoApp')
  .factory('Voices',function Voices() {
    //Need to init this synthesis so object exists
    //otherwise I dont have access to voices
    var speechSynthesisVoices = '';
    var speech = new window.SpeechSynthesisUtterance();
    speech.volume = 1.0;
    speech.rate = 1.0;
    speech.pitch = 1.0;
    speech.lang = 'EN';

    speechSynthesis.onvoiceschanged = function() {
        speechSynthesisVoices = speechSynthesis.getVoices();
        speech.voice = speechSynthesisVoices[3];
    };
    // Public API here
    return {
        get : function(){
            return speech ;
        }
    };
  });

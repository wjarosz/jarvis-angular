'use strict';

/**
 * @ngdoc service
 * @name jarvisAngularYoAppApp.Animation
 * @description
 * # Animation
 * Provider in the jarvisAngularYoAppApp.
 */
(function() {

    var animate = angular.module('adaptive.animation', []);

    animate.provider('Animation', function () {
        this.$get = function () {
            var c = document.getElementById('fft'),
                ctx = c.getContext('2d'),
                cw = c.width = 500,
                ch = c.height = 500,
                rand = function (a, b) {
                    return ((Math.random() * (b - a + 1)) + a);
                },
                dToR = function (degrees) {
                    return degrees * (Math.PI / 180);
                },
                circle = {
                    x: (cw / 2) + 5,
                    y: (ch / 2) + 22,
                    radius: 150,
                    speed: 2,
                    rotation: 0,
                    angleStart: 0,
                    angleEnd: 360,
                    hue: 192,
                    thickness: 1,
                    blur: 25
                },
                circleTwo = {
                    x: (cw / 2) + 5,
                    y: (ch / 2) + 22,
                    radius: 115,
                    speed: 1,
                    rotation: 0,
                    angleStart: 0,
                    angleEnd: 360,
                    hue: 180,
                    thickness: 1,
                    blur: 1
                },
                updateCircle = function (freq) {
                    if (circle.rotation < 360) {
                        circle.rotation += circle.speed;
                        circle.thickness = updateThicknes(freq);
                    } else {
                        circle.rotation = 0;
                    }
                },
                updateThicknes = function (circle) {
                    return (rand(0, 1000)) / 100;
                },
                updateCircleTwo = function () {
                    if (circleTwo.rotation < 360) {
                        circleTwo.rotation -= circleTwo.speed;
                        circleTwo.thickness = 30 + updateThicknes(circleTwo);
                    } else {
                        circleTwo.rotation = 0;
                        circleTwo.thickness = 0;
                    }
                },
                renderCircle = function () {
                    ctx.save();
                    ctx.translate(circle.x, circle.y);
                    ctx.rotate(dToR(circle.rotation));
                    ctx.beginPath();
                    ctx.arc(0, 0, circle.radius, dToR(circle.angleStart), dToR(circle.angleEnd), false);
                    ctx.lineWidth = circle.thickness;
                    ctx.strokeStyle = gradient1;
                    ctx.stroke();
                    ctx.restore();
                },
                renderSecondCircle = function () {
                    ctx.save();
                    ctx.translate(circleTwo.x, circleTwo.y);
                    ctx.rotate(dToR(circleTwo.rotation));
                    ctx.beginPath();
                    ctx.arc(0, 0, circleTwo.radius, dToR(circleTwo.angleStart), dToR(circleTwo.angleEnd), false);
                    ctx.lineWidth = circleTwo.thickness;
                    ctx.strokeStyle = gradient2;
                    ctx.stroke();
                    ctx.restore();
                },
                renderCircleBorder = function () {
                    ctx.save();
                    ctx.translate(circle.x, circle.y);
                    ctx.rotate(dToR(circle.rotation));
                    ctx.beginPath();
                    ctx.arc(0, 0, circle.radius + (circle.thickness / 2), dToR(circle.angleStart), dToR(circle.angleEnd), true);
                    ctx.lineWidth = 2;
                    ctx.strokeStyle = gradient2;
                    ctx.stroke();
                    ctx.restore();
                },
                clear = function () {
                    ctx.globalCompositeOperation = 'destination-out';
                    ctx.fillStyle = 'rgba(0, 0, 0, .1)';
                    ctx.fillRect(0, 0, cw, ch);
                    ctx.globalCompositeOperation = 'lighter';
                },
                loop = function (freq) {
                    clear();
                    updateCircle(freq);
                    updateCircleTwo(freq);
                    renderCircle();
                    renderCircleBorder();
                    renderSecondCircle();
                };

            ctx.shadowBlur = circle.blur;
            ctx.shadowColor = 'hsla(' + circle.hue + ', 80%, 60%, 1)';
            ctx.lineCap = 'round';

            var gradient1 = ctx.createLinearGradient(0, -circle.radius, 0, circle.radius * 2);
            gradient1.addColorStop(0, 'hsla(' + circle.hue + ', 96%, 25%, .25)');
            gradient1.addColorStop(1, 'hsla(' + circle.hue + ', 96%, 95%, 0)');

            var gradient2 = ctx.createLinearGradient(0, -circleTwo.radius, 0, circleTwo.radius * 2);
            gradient2.addColorStop(0, 'hsla(' + circleTwo.hue + ', 90%, 50%, .25)'); //hsla(179, 96%, 51%, 1.0);
            gradient2.addColorStop(1, 'hsla(' + circleTwo.hue + ', 90%, 95%, 1)');

            return {
                loop: function (freq) {
                    clear();
                    updateCircle(freq);
                    updateCircleTwo(freq);
                    renderCircle();
                    renderCircleBorder();
                    renderSecondCircle();
                }

            };

        };
    });
})();

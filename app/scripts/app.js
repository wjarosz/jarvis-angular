'use strict';

/**
 * @ngdoc overview
 * @name jarvisAngularYoApp
 * @description
 * # jarvisAngularYoApp
 *
 * Main module of the application.
 */
angular
  .module('jarvisAngularYoApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ui.bootstrap',
    'adaptive.speech',
    'angular-loading-bar',
    'adaptive.animation'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/config', {
        templateUrl: 'views/config.html',
        controller: 'ConfigCtrl'
      })
      .when('/command-edit/:name', {
        templateUrl: 'views/command-edit.html',
        controller: 'CommandeditCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  })
  .config(function($httpProvider) {
      $httpProvider.defaults.useXDomain = true;
      delete $httpProvider.defaults.headers.common['X-Requested-With'];
  })
  .constant({apiEndpoint : '//localhost:3000/'});

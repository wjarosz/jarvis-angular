'use strict';

/**
 * @ngdoc function
 * @name jarvisAngularYoAppApp.controller:RemoteCtrl
 * @description
 * # RemoteCtrl
 * Controller of the jarvisAngularYoAppApp
 */
angular.module('jarvisAngularYoApp')
  .controller('RemoteCtrl','Recorder', function ($scope, $socket) {

    console.log($socket);

    $socket.onopen = function() {
        console.log('open');
        //$('#execute').button({disabled: false});
    };

    $socket.onmessage = function(e) {

        var regEx = new RegExp(/Successfully written config file/ig);
        if ( e.data.match(regEx) ) {
                //$("#output").replaceWith('<div>Recording..</div>');
            console.log('start saving file');
        }
            //$('#output').append(colorReplace(e.data,true));
            //$('#output').stop().animate({
            //    scrollTop: $("#output")[0].scrollHeight
            //}, 800);
            //var end_of_output = /(\$|>)\s*$/;

            //if(end_of_output.test(e.data)){
            //$('#execute').button({disabled: false});
            //};
    };

    $socket.onclose = function() {
        console.log('close');
        //$('#execute').button({disabled: true});
    };

    $scope.recordingStarted = function(formData){
        console.log('button clicked!!!');
        var command = $scope.formData.command;
        $socket.send(command);
        //    $("input[name='command']").val('');
        //    $('#execute').button({disabled: true});
        //});

        //$('#enter').click(function(){
        //    command = 'cls';
        //    socket.send(command);
        //});
    };

    var colorReplace = function(input, replace) {

        var span = '<span style="';
        var color = 'color: ';
        var bold = ' font-weight: bold;';
        var underline = ' text-decoration: underline;';
        var spanFin = '">';
        var spanEnd = '</span>';
        var reset = /\033\[0+m/;

        input = input.replace(reset,'<span>');

        var replaceColors = {
            '31' : 'red;',
            '01;31' : 'red;' + bold,

            '32' : 'green;',
            '01;32' : 'green;' + bold,

            '33' : 'yellow;',
            '01;33' : 'yellow;' + bold,

            '34' : 'blue;',
            '01;34' : 'blue;' + bold,

            '35' : 'purple;',
            '01;35' : 'purple;' + bold,

            '36' : 'cyan;',
            '01;36' : 'cyan;' +bold,
            '04;36' : 'cyan;' +underline,

            '37' : 'white;',
            '01;37' : 'white;' +bold,

            '01;30' : bold,
            '01' : bold

        };


        for(var k in replaceColors ) {
            var re = new RegExp( '\\033\\[' + k + 'm', 'g' );
            var regStr  = spanEnd  + span + color + replaceColors[ k ] + spanFin;
            input = input.replace( re,regStr);
        }

        input = input.replace(/\033\[0+m/g,spanEnd);

        return input;
    };
  });

'use strict';

/**
 * @ngdoc function
 * @name jarvisAngularYoApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the jarvisAngularYoApp
 */
angular.module('jarvisAngularYoApp')
  .controller('MainCtrl', ['$scope', 'Voices', '$speechSynthetis', '$speechRecognition', '$speechCorrection', 'Speechutterancechunker', '$http',
        function ($scope, $voices, $speechSynthetis, $speechRecognition, $speechCorrection, $speechutterancechunker, $http) {
            var init = function() {

                $scope.LANG = 'en-US';

                $scope.msg = $voices.get();

                $speechRecognition.onstart(function () {

                    $scope.msg.text = 'Hello sir, How can I help you?';
                    $speechutterancechunker.speak($scope.msg.text);

                });

                $speechRecognition.listen();

                $speechRecognition.onerror(function (e) {
                    var error = (e.error || '');
                    console.log(error);
                });

                $speechRecognition.onUtterance(function (utterance) {
                    $scope.newUtterance = $scope.lastUtterance = utterance;
                });

                $scope.recognition = {};
                $scope.recognition['en-US'] = {
                    'sendRequest': {
                        'regex': /^(r2)|(are two)|(artoo) .+/gi,
                        'lang': 'en-US',
                        'call': function(utterance){
                            var parts = utterance.split(' ');
                            if (parts.length > 1) {
                                $http({
                                    method: 'POST',
                                    url: '//localhost:3000/jarvis-mob/',
                                    data: {data:utterance}  // pass in data as strings
                                })
                                .success(function (data) {
                                     console.log(data);
                                     $speechutterancechunker.speak(data);
                                });
                            }
                        }
                    }
                };

                $speechRecognition.listenUtterance($scope.recognition['en-US'].sendRequest);

            };

            init();
  }]);

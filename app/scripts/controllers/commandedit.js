'use strict';

/**
 * @ngdoc function
 * @name voictekApp.controller:CommandeditCtrl
 * @description
 * # CommandeditCtrl
 * Controller of the voictekApp
 */
angular.module('jarvisAngularYoApp')
  .controller('CommandeditCtrl', function ($scope, $route, $routeParams, $http, $window, $timeout) {

    $scope.hide = false;

    /**
     * Get current command details
     */
    var command = $routeParams.name.replace(/:/g,'');
    $scope.orginalCommandName = command;
    $scope.formData = {};
    var c = {'command':command};
    var res = $http.post('//localhost:3000/get-command', c);
    res.success(function(data, status, headers, config) {
        $scope.formData.command = data.message.command;
        $scope.formData.question = (data.message.question === undefined) ? '' : data.message.question;
        $scope.formData.exec = data.message.shell;
        $scope.formData.orginalCommandName =  data.message.command;
    });

    /**
     * Edit command
     * @param formData
     */
    $scope.editCommand = function(formData){
        $http({
            method: 'POST',
            url: '//localhost:3000/edit-command',
            data: $.param(formData),  // pass in data as strings
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }  // set the headers so angular passing info as form data (not request payload)
        })
        .success(function (data) {
            if (!data.success) {
                // if not successful, bind errors to error variables
                $scope.errorCommand = data.errors.commandError;
                $scope.errorShell = data.errors.shellError;
                $scope.message = data.message;
                $scope.formData.orginalCommandName = data.orginalCommandName;
                $scope.type = 'alert alert-danger';
            } else {
                // if successful, bind success message to message
                $scope.message = data.message;
                $scope.type = 'alert alert-success';
            }
        });
        $timeout(function(){
            $scope.message = '';
        }, 1000);
    };

    /**
     * Delete command
     * @param formData
     */
    $scope.deleteCommand = function(formData){
        $http({
            method: 'POST',
            url: '//localhost:3000/delete-command',
            data: $.param(formData),  // pass in data as strings
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }  // set the headers so angular passing info as form data (not request payload)
        })
        .success(function (data) {
            if (!data.success) {
                // if not successful, bind errors to error variables
                $scope.errorCommand = data.errors.commandError;
                $scope.errorShell = data.errors.shellError;
                $scope.message = data.message;
                $scope.type = 'alert alert-error';
            } else {
                // if successful, bind success message to message
                $scope.message = data.message;
                $scope.type = 'alert alert-success';
                $scope.hide = true;
            }
        });
        $timeout(function(){
            $scope.message = '';
            $window.history.back();
        }, 1000);

    };

    /**
     * Go Back button handler
     */
    $scope.goBack = function(){
        $window.history.back();
    };
  });

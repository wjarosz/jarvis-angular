'use strict';

/**
 * @ngdoc function
 * @name jarvisAngularYoApp.controller:ConfigCtrl
 * @description
 * # ConfigCtrl
 * Controller of the jarvisAngularYoApp
 */
angular.module('jarvisAngularYoApp')
  .controller('ConfigCtrl',['$scope','Fileupload','$http','$timeout','Recorder', function ($scope, $fileUpload, $http, $timeout, $socket) {

        $scope.initialState = true;
        $scope.buttonState = false;
        $scope.enterState = false;
        /**
         * File Uploader
         */
        $scope.uploadPlugin = function(myFile){
            var file = myFile;
            var uploadUrl = '//localhost:3000/';
            $fileUpload.uploadFileToUrl(file, uploadUrl);

        };

        /**
         * Create command
         * @param formData
         */
        $scope.createCommand = function(formData) {
            if(formData) {
                $http({
                    method: 'POST',
                    url: '//localhost:3000/create-command',
                    data: $.param(formData),  // pass in data as strings
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded' }  // set the headers so angular passing info as form data (not request payload)
                })
                    .success(function (data) {
                        console.log(data);
                        if (!data.success) {
                            // if not successful, bind errors to error variables
                            $scope.errorCommand = data.errors.commandError;
                            $scope.errorShell = data.errors.shellError;
                            $scope.message = data.message;
                            $scope.type = 'alert alert-danger';
                        } else {
                            // if successful, bind success message to message
                            $scope.message = data.message;
                            $scope.type = 'alert alert-success';
                        }
                    });
            }
            $timeout(function(){
                $scope.message = '';
            }, 1000);
        };

        /**
         * List all commands
         */
        $scope.refreshCommands = function(){
            $scope.fns = [];
            $http.get('//localhost:3000/get-all-commands/')
                .success(function(data, status) {
                    if (data && status === 200) {
                        data.forEach(function(e){
                            $scope.fns.push(e);
                        });
                    }
                });
        };

        /**
         * Get General Config Info
         */
        $scope.refreshConfig = function() {
            $scope.configFormData = {};
            $scope.configFormData.options = [
                {name: 'Alex'},
                {name: 'Albert'},
                {name: 'Bruce'},
                {name: 'Fred'},
                {name: 'Ralph'},
                {name: 'Zarvox'}
            ];
            $scope.configFormData.selectedOption = $scope.configFormData.options[0];
            $http.get('//localhost:3000/get-config/')
                .success(function (data, status) {
                    if (data && status === 200) {
                        angular.forEach(data, function (value, key) {
                            $scope.configFormData[key] = value;
                        });
                    }
                });
        };

        /**
         * Update general configuration
         * @param formData
         */
        $scope.editConfig = function(formData) {
            $http({
                method: 'POST',
                url: '//localhost:3000/edit-config/',
                data: $.param(formData),  // pass in data as strings
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' }  // set the headers so angular passing info as form data (not request payload)
            })
            .success(function (data) {
                if (!data.success) {
                    // if not successful, bind errors to error variables
                    $scope.errorWelcomeText = data.errors.welcomeText;
                    $scope.errorOwner = data.errors.owner;
                    $scope.errorEmail = data.errors.email;
                    $scope.errorPassword = data.errors.password;
                    $scope.errorVoice = data.errors.voice;
                    $scope.type = 'alert alert-danger';
                } else {
                    // if successful, bind success message to message
                    $scope.message = data.message;
                    $scope.type = 'alert alert-success';
                }
            });
            $timeout(function(){
                $scope.message = '';
            }, 1000);
        };

        /**
         * Sockjs - IR recorder outputs from Lirc recorder
         */
        $socket.onopen(
            function(){
                console.log('Socket is connected :D')
            }
        );

        $socket.onclose(
            function(){
                console.log('Socket is disconnected :(')
            }
        );

        $scope.outputs = [];

        $socket.onmessage(
            function($data){
                var output = {data : _colorReplace($data.data)};
                $scope.outputs.push(output);
                $('#output').stop().animate({
                    scrollTop: $("#output")[0].scrollHeight
                }, 800);

                var regButtons = new RegExp(/Please enter the name for the next button/igm);
                var regFinish = new RegExp(/Successfully written config file/igm);

                if ( $data.data.match(regButtons)) {
                    $scope.initialState = false;
                    $scope.buttonState = true;
                    $scope.enterState = false;
                }
                if ( $data.data.match(regFinish)) {
                    $scope.initialState = false;
                    $scope.buttonState = false;
                    $scope.enterState = true;
                    $scope.finish = true;
                }
            }
        );

        $scope.recordingStarted = function(data) {
            var remoteName = data.remoteName;

            if ($scope.initialState === true) {

                $socket.send('sudo /etc/init.d/lirc stop');
                $socket.send('sudo irrecord -d /dev/lirc0 /usr/share/remotes/lircd.conf.' + remoteName);

                $scope.initialState = false;
                $scope.buttonState = false;
                $scope.enterState = true;
            } else if ( $scope.buttonState === true ) {
                $socket.send(data.buttonName);
            } else if ( $scope.enterState === true ){
                $socket.send();
            } else if ( $scope.finish === true ) {
                _buildConfigFiles(remoteName);
            }
        };

        var _buildConfigFiles = function(remote){
            $socket.send('sudo bash -c "echo \'include /usr/share/remotes/lircd.conf.'+remote+'\' >> /etc/lirc/lircd.conf"');
            $socket.send('sudo /etc/init.d/lirc start');
        };


        var _colorReplace = function(input, replace) {

            var span = '<span style="';
            var color = 'color: ';
            var bold = ' font-weight: bold;';
            var underline = ' text-decoration: underline;';
            var spanFin = '">';
            var spanEnd = '</span>';
            var reset = /\]0;|\033\[0+m/igm;

            input = input.replace(reset,'<span>');

            var replaceColors = {
                '31' : 'red;',
                '01;31' : 'red;' + bold,

                '32' : 'green;',
                '01;32' : 'green;' + bold,

                '33' : 'yellow;',
                '01;33' : 'yellow;' + bold,

                '34' : 'blue;',
                '01;34' : 'blue;' + bold,

                '35' : 'purple;',
                '01;35' : 'purple;' + bold,

                '36' : 'cyan;',
                '01;36' : 'cyan;' +bold,
                '04;36' : 'cyan;' +underline,

                '37' : 'white;',
                '01;37' : 'white;' +bold,

                '01;30' : bold,
                '01' : bold

            };

            for(var k in replaceColors ) {
                var re = new RegExp( '\\033\\[' + k + 'm', 'g' );
                var regStr  = spanEnd  + span + color + replaceColors[ k ] + spanFin;
                input = input.replace( re,regStr);
            }

            input = input.replace(/\033\[0+m/g,spanEnd);

            return input;
        };


    }]);
